# README

O API de score jurídico é utilizado para consultar o status de uma pessoa física/jurídica com relação aos processos judiciais não sentenciados em uma determinada data de referência.

> **OBS**: Atualmente o API está utilizando dados da **Justica do Trabalho** de **São Paulo**.
> Em breve, será expandido para todos os estados e justiças. 


# Instruções

## Deployment:

Para implementar o API de score jurídico, basta modificar o script **deploy.R**. 
As únicas mudanças serão modificar a variável `PATH`, que armazena o endeço da pasta
onde está localizado os arquivos do API, e modificar a `port` que define em qual porta o API estará funcionando. 

```R
Path = "<diretorio_dos_arquivos_api>"

## Deploy the API:
pr <- plumber::plumb(file = "scripts/servico_api.R")
pr$run(port = <port>) 
```

Para implementar o API, basta abrir a linha de comando e executar: 

```sh
Rscript /<diretorio_dos_arquibos_api>/scripts/deploy.R 
```

Caso contrário, basta abrir sua IDE de preferência e executar o script **deploy.R**.

## Consultas:

O API de score jurídico possui dois endpoints: */score_judico_nome* e */score_juridico_documento*. O primeiro deverá ser utilizado se as consultas forem realizadas através do nome da réu, enquanto o último será utilizado caso o CPF/CNPJ do réu estiver disponível. 

Para gerar uma consulta, basta gerar uma requisição **POST** ao API no endpoint */score_juridico** com as seguintes informações:

 - *nome*: nome do réu;
 - *numero_documento*: radical do CPF/CNPJ;
 - *data_referencia*: data de referência para a consulta no formato `yyyy-mm-dd`. 
 
Para consultas realizadas através do nome, o campo *numero_documento* poderá vir vazio (`null`). 

Um exemplo de consulta ao API:

```sh
curl -X POST "http://<ip_address>:<port>/score_juridico_documento" -H "Content-Type: application/json" -d '
{
    "nome": ["XXXXXX"],
    "numero_documento": ["xx.xxx.xxx"],
    "data_referencia": ["yyyy-mm-dd"]
}'
```

onde `<ip_address>` e `<port>` são o endereço e a porta onde o API está implementado.

Na pasta */input* há arquivos exemplos do formato do json de input do API. 
 
## Output:

O output do API de risco jurídico será um **json** no seguinte formato:

```sh
{
    "reu": ["XXXXXXX"],
    "data_referencia": ["yyyy-mm-dd"],
    "indicador": ["XXXXX"]
}
```



