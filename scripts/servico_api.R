# Path = "/home/cleyton/score_juridico/"

## FUNÇÃO PARA CHECAR PACKAGES:
source(paste0(Path, "scripts/helper_functions.R"))


{
    # NOME DOS PACKAGES:
    packages <- c("data.table",
                  "dplyr",
                  "lubridate",
                  "tidyr",
                  "purrr",
                  "stringr",
                  "ranger",
                  "survival",
                  "jsonlite",
                  "purrr",
                  "caret", 
                  "plumber",
                  "MLmetrics",
                  "reticulate"
    )
    ## CHECANDO OS PACKAGES:
    check_packages(packages)
}


## LOADING LIBRARIES:
{
    suppressMessages(library(data.table))
    suppressMessages(library(dplyr))
    suppressMessages(library(ranger))
    suppressMessages(library(lubridate))
    suppressMessages(library(tidyr))
    suppressMessages(library(stringr))
    suppressMessages(library(caret))
    suppressMessages(library(purrr))
    suppressMessages(library(jsonlite))
    suppressMessages(library(MLmetrics))
    suppressMessages(library(reticulate))
}

# Setting up python ----
#Sys.setenv(LD_LIBRARY_PATH = "/opt/anaconda/lib/")
use_python("/opt/anaconda/bin/python", required = T)
source_python(paste0(Path, "scripts/query_por_documento.py"))
source_python(paste0(Path, "scripts/query_por_nome.py"))

##----------------------------------------------------------------------------##
# API landing page ----

#* @apiTitle Kurier Risk API

#* @apiDescription Welcome to the Kurier Risk API. We are currently serving version Beta. 

#* @apiVersion Beta




##----------------------------------------------------------------------------##
## Loading models ----

## justica trabalho:
justica_trabalho <- vector("list", 1)
names(justica_trabalho) <- c("sp")

##============================================================================##
## JUSTICA DO TRABALHO:

##----------------------------------------------------------------------------##
## SP:

## alocando array para modelos e parametros de SP:
sp <- vector("list", 2)
names(sp) <- c("models", "parameters")

## alocando array para modelos:
modelos <- vector("list", 2)
names(modelos) <- c("exito", "duracao_processo")

## alocando array para modelos de exito:
exito <- vector("list", 5)
names(exito) <- c("procedente", "parcialmente_procedente", "acordo", "improcedente", "outros")

##  reading models:
## exito:
exito[["procedente"]] <- readRDS(paste0(Path, "modelos/modelo_procedente.rds"))
exito[["parcialmente_procedente"]] <- readRDS(paste0(Path, "modelos/modelo_parcialmente_procedente.rds"))
exito[["acordo"]] <- readRDS(paste0(Path, "modelos/modelo_acordo.rds"))
exito[["improcedente"]] <- readRDS(paste0(Path, "modelos/modelo_improcedente.rds"))
exito[["outros"]] <- readRDS(paste0(Path, "modelos/modelo_outros.rds"))


##  duracao_processo:
#duracao_processo <- readRDS(paste0(Path, "modelos/justica_trabalho/sp/duracao_processo/modelo_duracao_processo.rds"))

## alocand os modelos na array de modelos:
modelos[["exito"]] <- exito
#modelos[["duracao_processo"]] <- duracao_processo
rm(exito)

## alocando a array de modelos na array de uf:
sp[['models']] <- modelos
rm(modelos)

## loading parameter:
parameter <- vector("list", 2)
names(parameter) <- c("exito", "duracao_processo")
parameter[["exito"]] <- readRDS(paste0(Path, "modelos/parameters.rds"))
#parameter[["duracao_processo"]] <- readRDS(paste0(Path, "modelos/justica_trabalho/sp/duracao_processo/parameters.rds"))

## alocando os parametros na array de uf:
sp[["parameters"]] <- parameter
rm(parameter)

## alocando a array de uf na array de justica:
justica_trabalho[["sp"]] <- sp

rm(sp)



##============================================================================##
# Query por documento ----

## JUSTICA DO TRABALHO
## SP:

#* @param req
#* @serializer contentType list(type="application/json")
#* @post /score_juridico_documento
#* @get /score_juridico_documento

function(req){
    
    ## receben  do a base de dados:
    input <- fromJSON(req$postBody,
                      simplifyDataFrame = F)
    
    
    # input <- fromJSON("input/claro.json", simplifyDataFrame = F)
    
    ## certificando que estamos pegando o cnpj radical:
    cnpj <- str_sub(input$numero_documento, start = 1, end = 8)
    data_referencia <- input$data_referencia
    
    ## check para data de referencia: o usuário tem que enviar uma data de 
    ## referencia no formato "yyyy-mm-dd":
    if( is.na(parse_date_time(data_referencia, orders = "ymd")) ){
        return(toJSON(list(message = "Erro de formatação na data de referencia")))
    }
    
    ## querying:
    ret = query_es_documento(cnpj, data_referencia = data_referencia, location = "data/", months = 18, cut_off = 20)

    # se nenhum processo for encontrado:
    if( !ret ){
        return(toJSON(list(reu = cnpj,
                           data_referencia = data_referencia,
                           indicador = "Processo nao encontrado para este réu"), 
                      dataframe = "columns", pretty = T))
    }

    ##------------------------------------------------------------------------##
    ## lendo base do grupo de comparação:
    file_con = file(paste0("data/", cnpj, ".json"), "r")
    data <- stream_in(file_con, simplifyDataFrame = F)
    close.connection(file_con)

    ## Ajeitando a base:
    data <- map(data, remove_fields)
    data <- rbindlist(data, fill = T)

    ## incluindo a data de referencia:
    data[, data_referencia := data_referencia]

    ## testando se o processo está sentenciado:
    data[, `:=`(dat_distribuicao = as_date(dat_distribuicao),
                dat_sentenca = as_date(dat_sentenca))]
    data[, data_referencia := ymd(data_referencia)]

    # removendo os "SENTENCIADO" que não possuem data de sentenca:
    data <- data[!(audit_status == "SENTENCIADO" & is.na(dat_sentenca))]

    ## processos que quero: os "NAO SENTENCIADOS" OR "SENTENCIADOS" com dat_sentenca > data_referencia:
    data <- data[(audit_status == "NAO SENTENCIADO") | (audit_status == "SENTENCIADO" & dat_sentenca > data_referencia)]

    ## organizando base:
    data <- organizando_base(base = data)

    bases <- organizando_base_modelos(base = data,
                                      parameter = justica_trabalho$sp$parameters)

    ## probability prediction:
    prev_procedente_prob <- predict(justica_trabalho$sp$models$exito$procedente,
                                    bases$base_indExito_sc$procedente,
                                    type = "prob")
    prev_procedente_prob
    setDT(prev_procedente_prob)

    prev_par_procedente_prob <- predict(justica_trabalho$sp$models$exito$parcialmente_procedente,
                                        bases$base_indExito_sc$parcialmente_procedente,
                                        type = "prob")
    prev_par_procedente_prob
    setDT(prev_par_procedente_prob)

    prev_acordo_prob <- predict(justica_trabalho$sp$models$exito$acordo,
                                bases$base_indExito_sc$acordo,
                                type = "prob")
    prev_acordo_prob
    setDT(prev_acordo_prob)

    prev_improcedente_prob <- predict(justica_trabalho$sp$models$exito$improcedente,
                                      bases$base_indExito_sc$improcedente,
                                      type = "prob")
    prev_improcedente_prob
    setDT(prev_improcedente_prob)

    prev_outros_prob <- predict(justica_trabalho$sp$models$exito$outros,
                                bases$base_indExito_sc$outros,
                                type = "prob")
    prev_outros_prob
    setDT(prev_outros_prob)

    prev <- data.table(select(prev_procedente_prob, procedente),
                       select(prev_par_procedente_prob, parcialmente_procedente),
                       select(prev_acordo_prob, acordo),
                       select(prev_improcedente_prob, improcedente),
                       select(prev_outros_prob, outros))

    prev_prob_gcompare <- round(map_df(prev, function(x) x/rowSums(prev)), 4)
    prev_prob_gcompare$num_npu <- bases$base_indExito_sc$parcialmente_procedente$num_npu
    setDT(prev_prob_gcompare)
    setcolorder(prev_prob_gcompare, "num_npu")
    prev_prob_gcompare

    ## merge
    prev_prob_gcompare <- merge(unique(data[, .(num_npu, reu, reu_cnae)]),
                                prev_prob_gcompare, by = "num_npu")

    ## calculando o score para cada reu:
    prev_prob_gcompare <- prev_prob_gcompare[, .(x = mean(procedente + parcialmente_procedente + acordo)),
                                             by = reu]
    prev_prob_gcompare[, reu := str_sub(reu, 1, 8)]

    ## lendo os cnpjs do grupo cnae para comparação:
    grupo_cnpj <- fread(paste0("data/", cnpj, "_grupo_cnae.csv"), colClasses = "character")

    ## pegando somente os cnpjs do grupo da cnae:
    #prev_prob_gcompare <- prev_prob_gcompare[reu %in% grupo_cnpj$cnpjs_cnae]
    prev_prob_gcompare <- merge(prev_prob_gcompare, grupo_cnpj, 
                                by.x = "reu", by.y = "cnpjs_cnae", 
                                all.y = T)
    prev_prob_gcompare[, x := ifelse(is.na(x), 0, x)][]
    
    ## calculando o z-score:
    prev_prob_gcompare[, score := (x - mean(x))/sd(x)][]

    ## ranking quantiles:
    res <- quantile(prev_prob_gcompare$score, c(0.25, .5, .75))
    res

    ## indicador:
    if( prev_prob_gcompare[reu == cnpj, score] <= res[[1]] ) {
        out <- "A"
    } else if( prev_prob_gcompare[reu == cnpj, score] > res[[1]] & prev_prob_gcompare[reu == cnpj, score] <= res[[2]] ) {
        out <- "B"
    } else if( prev_prob_gcompare[reu == cnpj, score] > res[[2]] & prev_prob_gcompare[reu == cnpj, score] <= res[[3]] ) {
        out <- "C"
    } else {
        out <- "D"
    }

    ##------------------------------------------------------------------------##
    ## organizando a saída
    return( toJSON(list(reu = cnpj,
                        data_referencia, data_referencia,
                        indicador = out), dataframe = "columns", pretty = T) )
}

##============================================================================##
# Query por nome ----

#* @param req
#* @serializer contentType list(type="application/json")
#* @post /score_juridico_nome
#* @get /score_juridico_nome

function(req){
    
    ## recebendo a base de dados:
    input <- fromJSON(req$postBody,
                      simplifyDataFrame = F)
    # input <- fromJSON("input/claro.json", simplifyDataFrame = F)
    
    ## certificando que estamos está em upper case:
    nome <- str_to_upper(input$nome)
    nome <- str_replace_all(nome, " {2,}", " ")
    nome <- str_replace_all(nome, "[[:punct:]]", "")
    nome <- str_trim(nome, "both")
    
    data_referencia <- input$data_referencia
    
    ## check para data de referencia: o usuário tem que enviar uma data de 
    ## referencia no formato "yyyy-mm-dd":
    if( is.na(parse_date_time(data_referencia, orders = "ymd")) ){
        return(toJSON(list(message = "Erro de formatação na data de referencia")))
    }
    
    ## querying:
    ret = query_es_nome(nome, data_referencia = data_referencia, location = "data/", months = 18, cut_off = 20)
    
    # se nenhum processo for encontrado:
    if( !ret ){
        return(toJSON(list(reu = nome,
                           data_referencia = data_referencia,
                           indicador = "Processo nao encontrado para este réu"), 
                      dataframe = "columns", pretty = T))
    }

    
    
    ##------------------------------------------------------------------------##
    ## lendo base do grupo de comparação:
    file_con = file(paste0("data/", str_replace_all(nome, "[^a-zA-Z]", ""), ".json"), "r")
    data <- stream_in(file_con, simplifyDataFrame = F)
    close.connection(file_con)
    
    ## Ajeitando a base:
    data <- map(data, remove_fields)
    data <- rbindlist(data, fill = T)
    
    ## incluindo a data de referencia:
    data[, data_referencia := data_referencia]
    
    ## testando se o processo está sentenciado:
    data[, `:=`(dat_distribuicao = as_date(dat_distribuicao),
                dat_sentenca = as_date(dat_sentenca))]
    data[, data_referencia := ymd(data_referencia)]
    
    # removendo os "SENTENCIADO" que não possuem data de sentenca:
    data <- data[!(audit_status == "SENTENCIADO" & is.na(dat_sentenca))]
    
    ## processos que quero: os "NAO SENTENCIADOS" OR "SENTENCIADOS" com dat_sentenca > data_referencia:
    data <- data[(audit_status == "NAO SENTENCIADO") | (audit_status == "SENTENCIADO" & dat_sentenca > data_referencia)]
    
    ## salvando o nome do reu:
    nome_reu <- unique(data[, .(num_npu, reus.nom_parte)])
    setnames(nome_reu, "reus.nom_parte", "reu")
    
    ## organizando base:
    data <- organizando_base(base = data)
    
    bases <- organizando_base_modelos(base = data,
                                      parameter = justica_trabalho$sp$parameters)
    
    ## probability prediction:
    prev_procedente_prob <- predict(justica_trabalho$sp$models$exito$procedente,
                                    bases$base_indExito_sc$procedente,
                                    type = "prob")
    prev_procedente_prob
    setDT(prev_procedente_prob)
    
    prev_par_procedente_prob <- predict(justica_trabalho$sp$models$exito$parcialmente_procedente,
                                        bases$base_indExito_sc$parcialmente_procedente,
                                        type = "prob")
    prev_par_procedente_prob
    setDT(prev_par_procedente_prob)
    
    prev_acordo_prob <- predict(justica_trabalho$sp$models$exito$acordo,
                                bases$base_indExito_sc$acordo,
                                type = "prob")
    prev_acordo_prob
    setDT(prev_acordo_prob)
    
    prev_improcedente_prob <- predict(justica_trabalho$sp$models$exito$improcedente,
                                      bases$base_indExito_sc$improcedente,
                                      type = "prob")
    prev_improcedente_prob
    setDT(prev_improcedente_prob)
    
    prev_outros_prob <- predict(justica_trabalho$sp$models$exito$outros,
                                bases$base_indExito_sc$outros,
                                type = "prob")
    prev_outros_prob
    setDT(prev_outros_prob)
    
    prev <- data.table(select(prev_procedente_prob, procedente),
                       select(prev_par_procedente_prob, parcialmente_procedente),
                       select(prev_acordo_prob, acordo),
                       select(prev_improcedente_prob, improcedente),
                       select(prev_outros_prob, outros))
    
    prev_prob_gcompare <- round(map_df(prev, function(x) x/rowSums(prev)), 4)
    prev_prob_gcompare$num_npu <- bases$base_indExito_sc$parcialmente_procedente$num_npu
    setDT(prev_prob_gcompare)
    setcolorder(prev_prob_gcompare, "num_npu")
    prev_prob_gcompare
    
    ## merge
    prev_prob_gcompare <- merge(nome_reu, prev_prob_gcompare, by = "num_npu")
    
    ## calculando o score para cada reu:
    prev_prob_gcompare <- prev_prob_gcompare[, .(x = mean(procedente + parcialmente_procedente + acordo)),
                                             by = reu]
    
    ## lendo os cnpjs do grupo cnae para comparação:
    grupo_cnpj <- fread(paste0("data/", str_replace_all(nome, "[^a-zA-Z]", ""), "_grupo_cnae.csv"), 
                        colClasses = "character", sep = ';')
    
    ## pegando somente os cnpjs do grupo da cnae:
    prev_prob_gcompare <- merge(separate_rows(prev_prob_gcompare, reu, sep = " __ "), 
                                grupo_cnpj, 
                                by.x = "reu", by.y = "cnpjs_cnae", 
                                all.y = T)
    ## pegando media por reu:
    prev_prob_gcompare <- prev_prob_gcompare[, .(x = mean(x)), reu]
    
    prev_prob_gcompare[, x := ifelse(is.na(x), 0, x)][]
    
    ## calculando o z-score:
    prev_prob_gcompare[, score := (x - mean(x))/sd(x)][]
    
    ## ranking quantiles:
    res <- quantile(prev_prob_gcompare$score, c(0.25, .5, .75))
    res
    
    ## indicador:
    if( prev_prob_gcompare[reu == nome, score] <= res[[1]] ) {
        out <- "A"
    } else if( prev_prob_gcompare[reu == nome, score] > res[[1]] & prev_prob_gcompare[reu == nome, score] <= res[[2]] ) {
        out <- "B"
    } else if( prev_prob_gcompare[reu == nome, score] > res[[2]] & prev_prob_gcompare[reu == nome, score] <= res[[3]] ) {
        out <- "C"
    } else {
        out <- "D"
    }
    
    ##------------------------------------------------------------------------##
    ## organizando a saída
    return( toJSON(list(reu = nome,
                        data_referencia = data_referencia,
                        indicador = out), dataframe = "columns", pretty = T) )
}


