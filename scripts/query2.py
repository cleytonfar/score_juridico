def query_es(cnpj, data_referencia, months = 12, location = "/mnt/dados/indice_serasa/data/", cut_off = 0.5):
  from elasticsearch import Elasticsearch
  import os
  import json
  import pandas as pd
  import numpy as np
  import math
  import time
  import urllib3
  urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
  from datetime import datetime
  import dateutil.relativedelta as delta_time
  
  
  ## Connection to ES:
  es = Elasticsearch(hosts = [{'host': '192.168.0.75', 'port': 9202,
  							 'timeout' : 30}],
  				   http_auth = ('cleyton.farias', 'Myr&&ov3'),
  				   scheme = "https",
  				   port = 443,
  				   use_ssl = True,
  				   verify_certs = False,
  				   ssl_show_warn = False)
  
  # cnpj = "40432544000147"
  # data_referencia = "2019-12-31"
  # months = 12
  # cut_off = 0.5
  # location = "/mnt/dados/indice_serasa/data/"
  
  ## montando as datas de referencia e a data de "months" antes da data_ref:
  data_ref = pd.to_datetime(data_referencia)
  data_lte = (data_ref - delta_time.relativedelta(months = months)).strftime("%Y-%m-%d")
  
  ##----------------------------------------------------------------------------##
  ## query quanto processos não-sentenciados o cnpj possui até data_lte:
  query_body = {
  	"size" : 10000,
  	"query" : {
  		"bool" : {
  			"filter" : {
  				"bool" : {
  					"must" : [
  						{"nested":
  						 {"path": "reus",
  						  "query": {"match_phrase": {"reus.num_documento.keyword": cnpj}}
  							}
  						},
  						{"match_phrase" : {"cod_uf" : "SP"}},
  						{"match_phrase" : {"nom_poder_jud" : "justica do trabalho"}},
  						{"range" : {"dat_distribuicao" : {"lte" : data_lte}}},
  						
  						{"bool": {
  						    "should": [
  						        {"range": {"dat_sentenca": {"gt": data_lte}}},
  						        {"match_phrase": {"audit_status": "nao sentenciado"}}
  						    ]
  						    
  						}
  						}
  						
  					]
  				}
  			}
  		}
  	},
  	"_source" : {
  		"includes" : [
  			"num_npu",
  			"dat_distribuicao",
  			"dat_sentenca",
  			"reus",
  			"audit_status"
  		]
  	},
  	"aggs": {
  		"count": {
  			"nested" : {"path": "reus"},
  			"aggs" : {
  				"by_cnpj" : {
  					"terms" : {"field" : "reus.num_documento.keyword",
  							   "size" : 100},
  				}
  			}
  		}
  	}
  }
  
  page = es.search(index = 'kurier_bra_*',
  				 body = query_body)
  
  out = pd.DataFrame(page["aggregations"]["count"]["by_cnpj"]["buckets"])
  out.rename(columns = {"doc_count" : "N", "key" : "ID"}, inplace = True)
      
  # pegando o número de processos que o cnpj tem em aberto até a data_lte:
  N = out.query("ID == @cnpj")["N"].item()
  
  ## pegando o cnae 2 ao qual esse cnpj pertence porque é mais específico:
  cnae = pd.DataFrame(page["hits"]["hits"][0]["_source"]["reus"]).dropna()
  cnae = cnae.query("num_documento == @cnpj")
  cnae = cnae["nom_cnae1"].item()
  print("CNAE: " + cnae)
  
  
  ##----------------------------------------------------------------------------##
  ## query qtos processos não-sentenciados as empresas do mesmo CNAE possuem até a
  ## data_lte:
  
  ## Query body:
  ## 1. Por cnae
  ## 2. Processos "nao sentenciados"
  ## 3. 'months' meses antes de 'data_ref'
  
  query_body = {
  	"size" : 10000,
  	"query" : {
  		"bool" : {
  			"filter" : {
  				"bool" : {
  					"must" : [
  						{"nested":
  						 {"path": "reus",
  						  "query": {"match_phrase": {"reus.nom_cnae1": cnae}}
  						 }
  						},
  						{"match_phrase" : {"cod_uf" : "SP"}},
  						{"match_phrase" : {"nom_poder_jud" : "justica do trabalho"}},
  						{"range" : {"dat_distribuicao" : {"lte" : data_lte}}},
  						
  						{"bool": {
  						    "should": [
  						        {"range": {"dat_sentenca": {"gt": data_lte}}},
  						        {"match_phrase": {"audit_status": "nao sentenciado"}}
  						    ]
  						    
  						}
  						}
  						
  					]
  				}
  			}
  		}
  	},
  	"_source" : {
  		"includes" : [
  			"dat_distribuicao",
  			"num_npu",
  			"reus",
  			"audit_status"
  		]
  	},
  	"aggs": {
  		"count": {
  			"nested" : {"path": "reus"},
  			"aggs" : {
  				"by_cnpj" : {
  					"terms" : {"field" :"reus.num_documento.keyword",
  							   "size" : 500},
  				}
  			}
  		}
  	}
  }
  
  page = es.search(index = 'kurier_bra_*',
  				 #scroll = '1s',
  				 body = query_body)
  
  out2 = pd.DataFrame(page["aggregations"]["count"]["by_cnpj"]["buckets"])
  out2.rename(columns = {"doc_count" : "N", "key" : "ID"}, inplace = True)
  
  ## calculando a diferenca percentual em número de processos abertos
  ## das empresas do mesmo cnae e o cnpj:
  out2 = out2.query("ID != 'NAO CLASSIFICADO'") # jogando fora os que possuem cnpj especificado como NAO CLASSIFICADO
  out2.eval("diff_perc = abs((N - @N)/@N)", inplace = True)
  out2.sort_values("diff_perc", inplace = True)
  out2.reset_index(inplace = True)
  
  ## pegando os cnpjs para o grupo de comparação:
  print("Grupo de Comparação para " + cnpj + " em " + data_lte)
  print(out2.query("diff_perc <= @cut_off").head(100))
  cnpjs_compare = out2.query("diff_perc <= @cut_off").head(100)["ID"].unique()
  print("\n")
  
  
  ##----------------------------------------------------------------------------##
  ## pegando os processos do cnpj e dos cnpjs dentro do grupo de comparação que estão
  ## aberto até data_ref:
  
  # salvando que é os cnpj do grupo cnae:
  pd.DataFrame({"cnpjs_cnae" : cnpjs_compare}).to_csv(location+cnpj+"_grupo_cnae.csv", index = False)
  
  ## juntando:
  cnpjs_compare = np.append(cnpjs_compare, cnpj)
  cnpjs_compare = np.unique(cnpjs_compare)
  
  f = open(location + cnpj + ".json", "w")
  
  for k in cnpjs_compare:
      
  	query_body = {
  	    "size" : 10000,
  		"query" : {
  			"bool" : {
  				"filter" : {
  					"bool" : {
  						"must" : [
  							{"nested":
  							 {"path": "reus",
  							  "query": {"match_phrase": {"reus.num_documento.keyword": k}}
  							 }
  							},
  							{"match_phrase" : {"cod_uf" : "SP"}},
  							{"match_phrase" : {"nom_poder_jud" : "justica do trabalho"}},
  							{"range" : {"dat_distribuicao" : {"lte" : data_ref}}},
  							
  							{"bool": {
						    "should": [
						        {"range": {"dat_sentenca": {"gt": data_ref}}},
						        {"match_phrase": {"audit_status": "nao sentenciado"}}
						    ]
						    
						}
						}
  							
  						]
  					}
  				}
  			}
  		},
  		"_source" : {
  			"excludes" : [
  			    "ind_exito_desc",
  				"nom_classe",
  				"id_and_sent",
  				"classificador_ia",
  				"dat_ultima_indexacao",
  				"id_pub_sent",
  				"qtd_vidautil",
  				"texto_pub_sent",
  				"ind_andamento",
  				"ind_exito",
  				"index_ano",
  				"ind_publicacao",
  				"nom_assuntos",
  				"@version",
  				"@timestamp",
  				"num_processo",
  				"dat_arquivamento",
  				"evidencia_sentenca",
  				"idprocesso",
  				"reu_unico",
  				"id_pub_evidencia",
  				"ind_status",
  				"text_and_sent",
  				"segunda_instancia",
  				"autor_unico",
  				"texto_and_sent",
  				"texto_andamento_sentenciado",
  				"dat_ultima_atualizacao_numeric"
  			]
  		}
  	}
  	
  	t = round(10000/500)
  	page = es.search(index = 'kurier_bra_*',
  					 scroll = str(t)+"s",
  					 body = query_body)
  	print("Total Found for %s in CNAE group: %d" %(k, page["hits"]["total"]["value"]))
  	## first scroll id:
  	sid = page['_scroll_id']
  	## size of the first batch:
  	scroll_size = len(page['hits']['hits'])
  	while (scroll_size > 0):
  		page_results = page["hits"]["hits"]
  		## saving into file line by line:
  		for proc in page_results:
  			_ = f.write(json.dumps(proc["_source"]) + os.linesep)
  		## scrolling:
  		t = max([round(scroll_size/500), 1])
  		page = es.scroll(scroll_id = sid,
  						 scroll = str(t)+"s")
  		## getting the new scroll id:
  		sid = page['_scroll_id']
  		# getting the new scross size:
  		scroll_size = len(page['hits']['hits'])
  	_ = es.clear_scroll(sid)
  
  
  f.close()
  print("\nDone!! Files are located at " + location + cnpj + ".\n")
  
